package _FileGenerator;


import java.io.IOException;
import java.net.URI;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;

public class Generator {

	private static final String[] DATA1 = {
		"right foot right and",
		"left foot left",
		"right back right back",
		"left back left",
		"right knee right knee",
		"left knee left",
		"right kick left kick",
		"clap and turn",
		"we go on the prowl each night",
		"like an alley cat",
		"lookin’ for some new delight",
		"like an alley cat"
		
	};
	
	
	private static final String[] DATA2 = {
		
		"I see trees of green, red roses too",
		"I see them bloom for me and you",
		"And I think to myself what a wonderful world",

		"I see skies of blue and clouds of white",
		"The bright blessed day, the dark sacred night",
		"And I think to myself what a wonderful world",

		"The colors of the rainbow so pretty in the sky",
		"Are also on the faces of people going by",
		"I see friends shaking hands saying how do you do",
		"They're really saying I love you",

		"I hear babies crying, I watch them grow",
		"They'll learn much more than I'll never know",
		"And I think to myself what a wonderful world",
		"Yes I think to myself what a wonderful world"
	};
	
	
	
	
	public static void main(String[] args)  {
		
		// args[0] is a number of string set (1 or 2) 
		String[] data = DATA1;
		if (args[0].equals("2")){data = DATA2;}
		
		// args[1] is a number of lines we want to get 
		Integer N = new Integer( args[1]);
		
		//args[2] is a path to save Snappy compressed SequenceFile  
		String pathString = args[2];
		
		
		
			try {
				
				// Setting environment 
				Configuration conf = new Configuration();
				FileSystem fs;
				fs = FileSystem.get(URI.create(pathString), conf);
				Path path = new Path(pathString);
				
				// Creating Writer
				Text value = new Text();
				IntWritable key = new IntWritable();
				SequenceFile.Writer writer = null;
				writer = SequenceFile.createWriter(fs, conf, path, IntWritable.class, Text.class,CompressionType.RECORD, new SnappyCodec());
				
				
				//Writing into Writer
				for (int i=0;i<N;i++){
					key.set(N-i);
					value.set(data[i%data.length]);
					//System.out.println(key+ " "+ value);
					writer.append(key, value);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}	
	}

}

