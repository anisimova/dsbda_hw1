package _1HW;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.io.IntWritable;



public class MyCombyner extends MapReduceBase
implements Reducer< IntWritable, Text, IntWritable,Text> {

public void reduce(IntWritable key, Iterator<Text> values,
		OutputCollector<IntWritable, Text> output, Reporter reporter) throws IOException
		 {
	// Combiner class is very similar with reduser class
	
	// Firstly, we set first word as longest one. It's length we put as maximum lenth 
	Text longestWord = new Text();
	longestWord.set(values.next());
	int max = longestWord.toString().length();
	
	//Here we lock at next words and compare it's length with maximum. If it's bigger, put this length as maximum. 
	//In this implementation we grap only one maximum lenth word
	//If there are more then one word whis max length, we'll get first one.
	while (values.hasNext()) {
		Text word = new Text();
		word.set(values.next());
	if (word.toString().length() > max) {
		max = word.toString().length();
		longestWord = word;
	}	}
	//collecting output
	output.collect(new IntWritable(max), longestWord);
	}
	
}