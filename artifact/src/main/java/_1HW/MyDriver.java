package _1HW;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.InputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.SequenceFileInputFormat;
import org.apache.hadoop.mapred.SequenceFileOutputFormat;



 public class MyDriver{
	 
	 public static void main(String[] args) throws IOException, URISyntaxException {
		 
		 // Creating Job
		 JobConf conf = new JobConf(MyDriver.class);
		 conf.setJobName("WordCount");
	
		 
		 
		String inputPath = args[0];
		String outputPath = args[1]; 
		Path inpur_dir = new Path(inputPath);
		Path output_dir = new Path(outputPath);
		
		
		
		// example:   "hdfs://localhost:8020/user/cloudera/output_data/"
		 FileInputFormat.addInputPath(conf, inpur_dir);
		 FileOutputFormat.setOutputPath(conf, output_dir);
		 
		 
		// Job settings
		 conf.setJarByClass(MyDriver.class);
		 conf.setMapperClass(MyMapper.class);
		 conf.setReducerClass(MyReduser.class);
		 conf.setCombinerClass(MyCombyner.class);
		 
		 
		 // !!!!!!!!!!!!!!!!!!  Input format settings is a magic. Don't ask me:"Why Object?". 
		 // It doesn't work other way !!!!!!!!!!!!!!!!!!
		 InputFormat<Object, Text> format= new SequenceFileInputFormat<Object, Text>();
		 conf.setInputFormat(format.getClass());
		 
		 
		 // Output format settings
		 conf.setOutputKeyClass(IntWritable.class);
		 conf.setOutputValueClass(Text.class);
		 conf.setOutputFormat(SequenceFileOutputFormat.class);
		 
		 
		 // If output_dir is not empty, we clean it
		 try {
			output_dir.getFileSystem(conf).delete(output_dir,true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		 
		 // Starting Job.
		 try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	 }
	 
	 
	 
	 
 }
