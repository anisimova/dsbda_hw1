package _1HW;



import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;


public class MyMapper extends MapReduceBase implements Mapper<Object, Text,  IntWritable,Text> {

	

	
	public void map(Object key, Text value,	OutputCollector<IntWritable,Text> output,
			Reporter reporter) throws IOException  {
		
		// In this mapper we split lines into separate words
		// Output looks like (1;"word")
			
		String line = value.toString();			
		StringTokenizer itr = new StringTokenizer(line);
		while (itr.hasMoreTokens()) {
			Text T =new Text(itr.nextToken()); // "T" is a word
			output.collect(new IntWritable(1),T);			
			}
			}
	}
	