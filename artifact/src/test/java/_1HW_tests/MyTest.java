package _1HW_tests;


import java.util.ArrayList;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.*;
import org.junit.*;

import _1HW.MyMapper;
import _1HW.MyReduser;



	public class MyTest {
	
		  MapDriver<Object, Text, IntWritable,Text> mapDriver;
		  ReduceDriver< IntWritable, Text, IntWritable,Text> reduceDriver;
		  MapReduceDriver<Object, Text, IntWritable,Text, IntWritable,Text> mapReduceDriver;
		 
		  @Before
		  public void setUp() {
		    MyMapper mapper = new MyMapper();
		    MyReduser reducer = new MyReduser();
		    mapDriver = MapDriver.newMapDriver(mapper);
		    reduceDriver = ReduceDriver.newReduceDriver(reducer);
		    mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
		  }
		 
		  @Test
		  public void testMapper() {
		    mapDriver.withInput(new Object(), new Text(
		        " 1 2 3"));
		    mapDriver.withOutput( new IntWritable(1),new Text("1"));
		    mapDriver.withOutput( new IntWritable(1),new Text("2"));
		    mapDriver.withOutput( new IntWritable(1),new Text("3"));
		    mapDriver.runTest();
		  }
		 
		  @Test
		  public void testReducer() {
		    ArrayList<Text> values = new ArrayList<Text>();
		    values.add(new Text("1234"));
		    values.add(new Text("12345"));
		    reduceDriver.withInput(new IntWritable(1), values);
		    reduceDriver.withOutput( new IntWritable(5),new Text("12345"));
		    reduceDriver.runTest();
	  }	
		   
		  @Test
		  public void testMapReduce() {
		    mapReduceDriver.withInput(new LongWritable(), new Text(
		              "111 22 3"));
		    mapReduceDriver.withOutput(new IntWritable(3),new Text("111") );
		    mapReduceDriver.runTest();
		  }
		}